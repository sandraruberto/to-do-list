package com.binus;

import org.junit.Test;

import static org.junit.Assert.*;

public class taskTest {
    @Test
    public void testGetTask(){
        task task = new task();
        int id = 1;
        String name = "Do dishes";
        String status = "DONE";
        task.setTask(id,name,status);
        assertEquals("1. Do dishes [DONE]",task.getTask());
    }
    @Test
    public void testGetTaskOnNullReferences(){
        task task = new task();
        assertEquals("no task",task.getTask());
    }
}