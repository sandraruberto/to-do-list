package com.binus;

import org.junit.Test;

import static org.junit.Assert.*;

public class todolistTest {
    @Test
    public void testAddTask(){
        todolist todo = new todolist();
        todo.addTask(1,"Do dishes","DONE");
        assertEquals("1. Do dishes [DONE]", todo.taskList.get(0).getTask());
    }

    @Test
    public void testTaskList(){
        todolist todo = new todolist();
        todo.addTask(1,"Do dishes","DONE");
        todo.addTask(2,"Learn Java","NOT DONE");
        todo.addTask(3,"Learn TDD","NOT DONE");
        assertEquals("1. Do dishes [DONE]\n2. Learn Java [NOT DONE]\n3. Learn TDD [NOT DONE]", todo.getTask());
    }

}