package com.binus;

import java.util.ArrayList;

public class todolist {

    ArrayList<task> taskList = new ArrayList<>();

    public void addTask(int id, String name, String status) {
        task task = new task();
        task.setTask(id, name, status);
        taskList.add(task);
    }

    public String getTask(){
        String result = "";
        for(int i=0; i<taskList.size();i++){
            result += taskList.get(i).getTask();
            if(i<taskList.size()-1) {
                result += "\n";
            }
        }
        return result;
    }
}
