package com.binus;

public class task {
    private int taskId;
    private String name;
    private String status;

    public String getTask() {
        if(name==null || taskId==0 || status==null){
            return "no task";
        }
        return taskId + ". " + name + " [" + status + "]";
    }

    public void setTask(int id, String name, String status) {
        this.taskId = id;
        this.name = name;
        this.status = status;
    }
}
